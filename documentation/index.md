# Double Pendulum

## Introduction

From https://en.wikipedia.org/wiki/Double_pendulum:

"In physics and mathematics, in the area of dynamical systems, a double pendulum is a pendulum with another pendulum
attached to its end, is a simple physical system that exhibits rich dynamic behavior with a strong sensitivity to
initial conditions. The motion of a double pendulum is governed by a set of coupled ordinary differential equations
and is chaotic."

## Usage

### Raw data

```python
from double_pendulum import DoublePendulumDataset

dataset = DoublePendulumDataset()
df = dataset.get_df()
print(dataset.get_csv_data())
```

### Pytorch Data Loader

```python
from double_pendulum import DoublePendulumDataset

dataset = DoublePendulumDataset()
print('example of data: ', dataset[0])
# prints (array([ 0.9749239 ,  1.6820338 , -0.22253847,  0.48456526], dtype=float32), array([-0.42090693, -1.3463068 , -0.90710384, -1.286096  ], dtype=float32))

train_loader, test_loader = dataset.get_train_test_data_loaders(batch_size=100)
```

Both ```train_loader``` and ```test_loader``` are pytorch data
loaders: https://pytorch.org/docs/stable/data.html#torch.utils.data.DataLoader

## Dataset Description

Based on: https://scipython.com/blog/the-double-pendulum/
![double-pendulum-geometry](double-pendulum-geometry.svg)

```python
from double_pendulum import DoublePendulumDataset

DoublePendulumDataset(min_shift=999, max_shift=1000, max_time=1000, dt=0.001)
```

Here are the data set details:

- Label dimension: 4. It corresponds to the x and y positions of the blue and red mass.
- Feature dimension: **if shift values are provided** the number of features is 4*(```max_shift```-```min_shift```).
  The features correspond to past positions of each mass.
- Number of points: the approximate number of points is equal to ```max_time```/```dt```.

The labels describe the positions of the masses at a time t. The features correspond to all the positions of the
masses from t-```max_shift``` to t-```min_shift```.

*Note: it would be possible to increase the dimension of the labels by adding new masses (triple pendulum for example).*

## Chaotic motion

The double pendulum undergoes chaotic motion, and shows a sensitive dependence on initial conditions.
This implies that:

- for small times/shifts: the labels are easy to predict
- for large times/shifts: the labels are very hard to predict

![Demonstrating_Chaos_with_a_Double_Pendulum](Demonstrating_Chaos_with_a_Double_Pendulum.gif)

*From wikipedia: "Three double pendulums with near identical initial conditions diverge over time displaying the chaotic
nature of the
system."*

The chaotic motion can be adjusted by changing the initial conditions of the double pendulum. For instance:
- ```initial_condition=(.5, 0., -2.5, 0.)``` will trigger a chaotic double pendulum behavior
- ```initial_condition=(0.1, 0, 0.1, 0)``` behaves very similarly to a single pendulum

## Energy drift over time

In principle, the number of data points produced by the simulation can be arbitrary large.
In practice, solving the ODE introduces an error that cumulates over time. This error introduces an energy drift.
Over a too large period of time, this energy drift changes the behavior of the physical system which means that the
function labels=function(features) changes over time.
In case this effect is noticeable, a python ```AssertionError``` will is raised.

## Example of prediction

Here is an example of the prediction of the x position of the first mass. The blue colored area corresponds to the
uncertainty of the prediction.

![prediction_example](prediction_example.png)

In this example, the number of features is 40 (```min_shift=990``` and ```max_shift=1000```).