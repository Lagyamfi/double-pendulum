import numpy as np
import pytest

from double_pendulum import DoublePendulumDataset
from double_pendulum.double_pendulum import DoublePendulum


class TestDoublePendulum:
    @staticmethod
    def test_min_shift_smaller_than_max_shift():
        with pytest.raises(AssertionError):
            DoublePendulumDataset(min_shift=2, max_shift=1, max_time=10)

    @staticmethod
    def test_check_energy_conservation_raises_assertion_error():
        position_1 = np.array([0., 0., 0., 0.])
        position_2 = np.array([0., 0., 0., 0.])
        DoublePendulum(mass_1=1., mass_2=1.)._check_energy_conservation(position_1, position_2)

        position_2 = np.array([0., 0., 0., 1.])
        with pytest.raises(AssertionError):
            DoublePendulum(mass_1=1., mass_2=1.)._check_energy_conservation(position_1, position_2)

    @staticmethod
    @pytest.mark.parametrize('min_shift, max_shift', [(1, 2), (1, 10), (10, 20)])
    def test_get_item_separates_features_and_labels(min_shift, max_shift):
        dataset = DoublePendulumDataset(min_shift=min_shift, max_shift=max_shift, max_time=10)
        feature, labels = dataset[0]
        assert len(feature) == 4 * (max_shift - min_shift)
        assert len(labels) == 4
        df = dataset._df
        assert np.float32(df.loc[max_shift + 1, f'lag(x1,{min_shift})']) == feature[0]
        assert np.float32(df.loc[max_shift + 1, 'x1']) == labels[0]
        assert np.float32(df.loc[max_shift + 1 + min_shift, f'lag(x1,{min_shift})']) == labels[0]

    @staticmethod
    @pytest.mark.parametrize('max_time, max_shift', [(1, 2), (1, 10), (10, 20)])
    def test_data_set_size(max_time, max_shift):
        dataset = DoublePendulumDataset(min_shift=1, max_shift=max_shift, max_time=max_time, dt=0.001)
        assert len(dataset) == int(max_time / 0.001 - max_shift)

    @staticmethod
    def test_get_raw_data():
        dataset = DoublePendulumDataset(max_time=1, dt=1, min_shift=None, max_shift=None)
        df = dataset.get_df()
        assert tuple(df.columns) == ('x1', 'x2', 'y1', 'y2')
        csv = dataset.get_csv_data()
        assert csv == ('x1,x2,y1,y2\n'
                       '0.479425538604203,-0.11904660549975354,-0.8775825618903728,-0.07643894634343906\n'
                       '-0.7707558058497386,-0.18279458662797932,-0.637130667719676,-1.4460197853935772\n')
