import numpy as np
from scipy.integrate import odeint


class DoublePendulum:
    def __init__(self, mass_1, mass_2, length_1=1, length_2=1, maximum_energy_drift=0.01,
                 gravitational_constant_g=9.81, initial_condition=(.5, 0., -2.5, 0.)):
        self.maximum_energy_drift = maximum_energy_drift
        self.gravitational_constant_g = gravitational_constant_g
        self.length_1 = length_1
        self.length_2 = length_2
        self.mass_1 = mass_1
        self.mass_2 = mass_2
        self.initial_condition = initial_condition

    def generate_data(self, max_time=1000, dt=0.001):
        """
        Args:
            max_time: the end time of the simulation
            dt: time step
            initial_condition: initial conditions of theta1, dtheta1/dt, theta2, dtheta2/dt.

        Returns:
            x1, x2, y1, y2: position of the first and second mass
        """
        initial_condition = np.asarray(self.initial_condition)
        t = np.arange(0, max_time + dt, dt)
        y = odeint(self._deriv, initial_condition, t, args=(self.length_1, self.length_2, self.mass_1, self.mass_2))
        y = np.asarray(y)
        self._check_energy_conservation(y, initial_condition)
        theta1, theta2 = y[:, 0], y[:, 2]
        x1, x2, y1, y2 = self._convert_to_cartesian_coordinates(theta1, theta2)
        return x1, x2, y1, y2

    def _deriv(self, y, t, length_1, length_2, mass_1, mass_2):
        """
        Return the first derivatives of y = theta1, z1, theta2, z2.
        """
        theta1, z1, theta2, z2 = y

        c, s = np.cos(theta1 - theta2), np.sin(theta1 - theta2)

        theta1dot = z1
        z1dot = (mass_2 * self.gravitational_constant_g * np.sin(theta2) * c - mass_2 * s * (
                length_1 * z1 ** 2 * c + length_2 * z2 ** 2) -
                 (mass_1 + mass_2) * self.gravitational_constant_g * np.sin(theta1)) / length_1 / (
                        mass_1 + mass_2 * s ** 2)
        theta2dot = z2
        z2dot = ((mass_1 + mass_2) * (length_1 * z1 ** 2 * s - self.gravitational_constant_g * np.sin(
            theta2) + self.gravitational_constant_g * np.sin(theta1) * c) +
                 mass_2 * length_2 * z2 ** 2 * s * c) / length_2 / (mass_1 + mass_2 * s ** 2)
        return theta1dot, z1dot, theta2dot, z2dot

    def _compute_energy(self, y):
        """
        Return the total energy of the system.
        """
        th1, th1d, th2, th2d = y.T
        V = -(self.mass_1 + self.mass_2) * self.length_1 * self.gravitational_constant_g * np.cos(
            th1) - self.mass_2 * self.length_2 * self.gravitational_constant_g * np.cos(th2)
        T = 0.5 * self.mass_1 * (self.length_1 * th1d) ** 2 + 0.5 * self.mass_2 * (
                (self.length_1 * th1d) ** 2 + (self.length_2 * th2d) ** 2 +
                2 * self.length_1 * self.length_2 * th1d * th2d * np.cos(
            th1 - th2))
        return T + V

    def _convert_to_cartesian_coordinates(self, theta1, theta2):
        """
        Convert to Cartesian coordinates of the two bob positions.
        """
        x1 = self.length_1 * np.sin(theta1)
        y1 = -self.length_1 * np.cos(theta1)
        x2 = x1 + self.length_2 * np.sin(theta2)
        y2 = y1 - self.length_2 * np.cos(theta2)
        return x1, x2, y1, y2

    def _check_energy_conservation(self, y, y0):
        """
        Check that the calculation conserves total energy to within some tolerance.
        """
        initial_energy = self._compute_energy(y0)
        current_energies = self._compute_energy(y)
        assert np.all(np.abs(current_energies - initial_energy) < self.maximum_energy_drift)
