# Double Pendulum

## Description

Library that generates a time series dataset based on pytorch's [DataSets](https://pytorch.org/docs/stable/data.html)
and [DataLoaders](https://pytorch.org/docs/stable/data.html#torch.utils.data.DataLoader).

## Visuals

![double_pendulum](documentation/double_pendulum.gif)

## Installation

```shell
pip install git+https://gitlab.com/eijikawasaki/double-pendulum.git
```

## Usage

```python
from double_pendulum import DoublePendulumDataset

dataset = DoublePendulumDataset()
train_loader, test_loader = dataset.get_train_test_data_loaders(batch_size=100)
for data in iter(train_loader):
    ...
```

## Author

Eiji Kawasaki

## License

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software. You can use,
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 
